/**
 * @file		tratarstring.h
 * @brief		Funçoes para o tratamento de uma string. Remoçao de acentos e espaços.
 * @details		As funcoes definidas neste cabecalho dizem respeito a conversao
 *				da string para letras minusculas, remocao de espacos em branco e
 *				remocao de caracteres de pontuacao.
 * @author		Rodolpho Erick
 * @since		09/05/2017
 * @data		12/05/2017
 */

#ifndef TRATARSTRING_H
#define	TRATARSTRING_H

#include <string>
using std::string;

/**
 * @brief Funcao que converte todos os caracteres para letra minuscula
 * @param s String a ser convertida
 */
void minusculas(string &s);

/**
 * @brief Funcao que remove espacos de uma string
 * @param s String com espacos a remover
 */
void remove_espacos(string &s);

/**
 * @brief Funcao que remove sinais de pontuacao de uma string
 * @param s String com sinais de pontuacao a remover
 */
void remove_pontuacao(string &s);

/**
 * @brief Funcao que remove sinais de acentuacao de uma string
 * @param s String com sinais de acentuacao a remover
 */
void remove_acentuacao(string &s);

/**
 * @brief Funcao que trata uma string, removendo espacos em branco
 * @param s String a ser tratada
 */
void tratar_string(string &s);

#endif