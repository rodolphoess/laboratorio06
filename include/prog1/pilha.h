/**
 * @file		pilha.h
 * @brief		Funçoes para criaçao, inserçao e remoçao de um elemento em uma pilha.
 * @details		As funcoes definidas neste cabecalho dizem respeito a criaçao de uma
 *				pilha, a inserçao de um elemento no topo da pilha, e a remocao desse
 *				mesmo elemento. 
 * @author		Rodolpho Erick	(rodolphoerick90@gmail.com)
 * @since		09/05/2017
 * @date		12/05/2017
 */

#ifndef PILHA_H
#define PILHA_H

#include <iostream>

using namespace std;

/**
 * @brief Estrutura representando uma pilha.
 */
struct Pilha {
	int *elemento;
	int topo; 
	int nElementos;
};

/**
 * @brief Funcao que a pilha de acordo com o tamanho da string.
 * @param *p Estrutura do tipo pilha.
 * @param tamanho Tamanho que a estrutura terá para armazenar a string.
 */
void cria_pilha (struct Pilha *p, int tamanho);

/**
 * @brief Insere um elemento no topo da pilha. Caso a inserção, a função retorna 1. Se a pilha estiver cheia, retorna 0.
 * @param *p Estrutura do tipo pilha.
 * @param n  Recebe o indice de uma letra da palavra e a coloca no topo da pilha.
 */
int push (struct Pilha *p, int n);

/**
 * @brief Remove o elemento do topo da pilha. 
 * @param *p Estrutura do tipo pilha.
 * @param n  Recebe o indice da letra que esta no topo da pilha.
 */
int pop (struct Pilha *p, int *n);

#endif