# Exemplo completo de um Makefile, separando os diferentes elementos da aplicacao
# como codigo (src), cabecalhos (include), executaveis (build), bibliotecas (lib), etc.
# Cada elemento e alocado em uma pasta especifica, organizando melhor seu codigo fonte.
#
# Algumas variaveis sao usadas com significado especial:
#
# $@ nome do alvo (target)
# $^ lista com os nomes de todos os pre-requisitos sem duplicatas
# $< nome do primeiro pre-requisito
#

# Comandos do sistema operacional
# Linux: rm -rf 
# Windows: cmd //C del 
RM = rm -rf 

# Compilador
CC=g++ 

# Variaveis para os subdiretorios
LIB_DIR=./lib
INC_DIR=./include
SRC_DIR=./src
OBJ_DIR=./build
DOC_DIR=./doc
TEST_DIR=./test

# Nome do programa
PROG1=palindromo

# Opcoes de compilacao
CFLAGS=-Wall -pedantic -ansi -std=c++11 -I.

# Garante que os alvos desta lista nao sejam confundidos com arquivos de mesmo nome
.PHONY: init all clean doxy debug doc

# Define o alvo (target) para a compilacao completa.
# Ao final da compilacao, remove os arquivos objeto.
all: $(PROG1) 

debug: CFLAGS += -g -O0
debug: $(PROG1) 


# Alvo (target) para a criação da estrutura de diretorios
# necessaria para a geracao dos arquivos objeto 
init:
	@mkdir -p $(OBJ_DIR)/prog1
	@mkdir -p $(BIN_DIR)/prog1


# Alvo (target) para a construcao do executavel sequencia
# Define os arquivos *.o como dependencias
$(PROG1): CFLAGS += -I$(INC_DIR)/prog1
$(PROG1): BIN_DIR=./bin/prog1
$(PROG1): $(OBJ_DIR)/prog1/pilha.o $(OBJ_DIR)/prog1/tratarstring.o $(OBJ_DIR)/prog1/main.o
	@echo "============="
	@echo "Ligando o alvo $@"
	@echo "============="
	$(CC) $(CFLAGS) -o $(BIN_DIR)/$@ $^
	@echo "+++ [Executavel 'palindromo' criado em $(BIN_DIR)] +++"
	@echo "============="

# Alvo (target) para a construcao do objeto pilha.o
# Define os arquivos pilha.cpp e pilha.h como dependencias.
$(OBJ_DIR)/prog1/pilha.o: $(SRC_DIR)/prog1/pilha.cpp $(INC_DIR)/prog1/pilha.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo (target) para a construcao do objeto tratarstring.o
# Define os arquivos tratarstring.cpp e tratarstring.h como dependencias.
$(OBJ_DIR)/prog1/tratarstring.o: $(SRC_DIR)/prog1/tratarstring.cpp $(INC_DIR)/prog1/tratarstring.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo (target) para a construcao do objeto main.o
# Define os arquivos main.cpp como dependencias.
$(OBJ_DIR)/prog1/main.o: $(SRC_DIR)/prog1/main.cpp
	$(CC) -c $(CFLAGS) -o $@ $<


# Alvo (target) para a geração automatica de documentacao usando o Doxygen.
# Sempre remove a documentacao anterior (caso exista) e gera uma nova.
doxy:
	$(RM) $(DOC_DIR)/*
	doxygen Doxyfile

# Alvo (target) usado para limpar os arquivos temporarios (objeto)
# gerados durante a compilacao, assim como os arquivos binarios/executaveis.
clean:
	$(RM) $(BIN_DIR)/*
	$(RM) $(OBJ_DIR)/*