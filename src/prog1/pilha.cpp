/**
 * @file		pilha.cpp
 * @brief		Funçoes para criaçao, inserçao e remoçao de um elemento em uma pilha.
 * @details		As funcoes definidas neste cabecalho dizem respeito a criaçao de uma
 *				pilha, a inserçao de um elemento no topo da pilha, e a remocao desse
 *				mesmo elemento.
 * @author		Rodolpho Erick	(rodolphoerick90@gmail.com)
 * @since		09/05/2017
 * @data		12/05/2017
 */

#include "pilha.h"

/**
 * @brief Funcao que a pilha de acordo com o tamanho da string.
 * @param *p Estrutura do tipo pilha.
 * @param tamanho Tamanho que a estrutura terá para armazenar a string.
 */
void cria_pilha (struct Pilha *p, int tamanho) {
	 p -> nElementos = tamanho;
	 p -> elemento = new int[tamanho];
	 p -> topo = -1;
}

/**
 * @brief Insere um elemento no topo da pilha. Caso a inserção, a função retorna 1. Se a pilha estiver cheia, retorna 0.
 * @param *p Estrutura do tipo pilha.
 * @param n  Recebe o indice de uma letra da palavra e a coloca no topo da pilha.
 */
int push (struct Pilha *p, int n) {
	 if (p -> topo == (p -> nElementos - 1)) {
		 cerr << "\n\nERRO: Pilha cheia!!" << endl;		 
		 return 0;
	 } else {
		 p -> topo++;
		 p -> elemento[p -> topo] = n;
		 return 1;
	 }
}

/**
 * @brief Remove o elemento do topo da pilha. 
 * @param *p Estrutura do tipo pilha.
 * @param n  Recebe o indice da letra que esta no topo da pilha.
 */
int pop (struct Pilha *p, int *n) {
	 if (p -> topo == -1) {
		 cerr << "\n\nERRO: Pilha vazia!!" << endl;		 
		 return 0;
	 } else {
		 *n = p -> elemento[p -> topo];
		 p -> topo--;
		 return 1;
	 }
}
