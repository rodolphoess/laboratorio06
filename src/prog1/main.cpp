/**
 * @file		main.cpp
 * @brief		Funçao principal para verificacao de um palindromo utilizando pilha.
 * @author		Rodolpho Erick	(rodolphoerick90@gmail.com)
 * @since		09/05/2017
 * @data		12/05/2017
 */

#include <iostream>
#include <string>

#include "pilha.h"
#include "tratarstring.h"

using namespace std;

/** @brief Funcao principal */
int main() {
	string palavra;	
	int n, y = 0;
	
	cout << "Digite uma palavra ou frase: ";
	getline(cin, palavra);	

	string palavraTratada = palavra;
	tratar_string(palavraTratada);
	cout << palavraTratada << endl;

	int tamanho = (int)palavraTratada.length();

	Pilha *palindromo;
	cria_pilha(palindromo, tamanho);	
	 
	for(int i = 0; i < (int)palavraTratada.length(); i++) {		
		push(palindromo, (int)palavraTratada[i]);
	}
	 
 	for(int i = 0; i < (int)palavraTratada.length(); i++) {
		pop(palindromo, &n);

		if (n != (int)palavraTratada[i]) {
		 	y = 1;
			break;
		}
	}

	if (y == 1) {	 	
		cout << "\n\"" << palavra << "\"" << " nao eh palindromo!" << endl;
	} else {
	 	cout << "\n\"" << palavra << "\"" <<  " eh palindromo!" << endl;
	}	

	delete palindromo;

	return 0;
}