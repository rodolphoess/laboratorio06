/**
 * @file		tratarstring.cpp
 * @brief		Funçoes para o tratamento de uma string. Remoçao de acentos e espaços.
 * @details		As funcoes definidas neste cabecalho dizem respeito a conversao
 *				da string para letras minusculas, remocao de espacos em branco e
 *				remocao de caracteres de pontuacao.
 * @author		Rodolpho Erick
 * @since		09/05/2017
 * @data		12/05/2017
 */

#include <cctype>
using std::ispunct;
using std::tolower;

#include <iostream>
using std::cin;
using std::cout;
using std::endl;

#include <string>
using std::string;

#include "tratarstring.h"

 /**
 * @brief Funcao que converte todos os caracteres para letra minuscula
 * @param s String a ser convertida
 */
void minusculas(string &s) {
	for (int i = 0; i < (int)s.length(); i++) {
		s[i] = tolower(s[i]);
	}
}

/**
 * @brief Funcao que remove espacos de uma string
 * @param s String com espacos a remover
 */
void remove_espacos(string &s) {	
	for (int i = 0; i < (int)s.length(); i++) {
		if (s[i] == ' ' || s[i] == '\t' || s[i] == '\n') {
			s.erase(i, 1);
			i--;
		}
	}
}

/**
 * @brief Funcao que remove sinais de pontuacao de uma string
 * @param s String com sinais de pontuacao a remover
 */
void remove_pontuacao(string &s) {
	for (int i = 0; i < (int)s.length(); i++) {
		if (ispunct(s[i])) {
			s.erase(i, 1);			
		}
	}
}

/**
 * @brief Funcao que remove sinais de acentuacao de uma string
 * @param s String com sinais de acentuacao a remover
 */
void remove_acentuacao(string &s) {
	int i = 0, tam = 0, size = 0;
	string comAcentos = "ÄÅÁÂÀÃäáâàãÉÊËÈéêëèÍÎÏÌíîïìÖÓÔÒÕöóôòõÜÚÛüúûùÇç";
	string semAcentos = "AAAAAAaaaaaEEEEeeeeIIIIiiiiOOOOOoooooUUUuuuuCc";

	tam = comAcentos.length();

	size = s.length();

	while(i < size) {
		int cont = 0;
		for(int j = 0; j <  tam; j += 2) {
			if(s[i] == comAcentos[j] && s[i + 1] == comAcentos[j + 1]) {
				s = s.replace(i, 1, semAcentos.substr(cont, 1) ) ;
				s.erase(i + 1, 1);
				i++;
			}
			cont++;
		}
		i++;
	}
}

/**
 * @brief Funcao que trata uma string, removendo espacos em branco
 * @param s String a ser tratada
 */
void tratar_string(string &s) {
	minusculas(s);			// Conversao da string para minusculas
	remove_espacos(s);		// Remocao de espacos
	remove_pontuacao(s);	// Remocao de pontuacao
	remove_acentuacao(s);	// Remocao de acentuacao 
}
